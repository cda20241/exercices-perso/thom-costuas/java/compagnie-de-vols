import java.util.ArrayList;
import java.util.List;

/**
 * Classe représentant un passager.
 */
public class Passager {

    // Attributs de la classe Passager
    private String nom;
    private String prenom;
    private String dateNaissance;
    private String telephone;
    private String statut;

    /**
     * Constructeur de la classe Passager.
     *
     * @param nom           Le nom du passager.
     * @param prenom        Le prénom du passager.
     * @param dateNaissance La date de naissance du passager.
     * @param telephone     Le numéro de téléphone du passager.
     * @param statut        Le statut du passager.
     */
    public Passager(String nom, String prenom, String dateNaissance, String telephone, String statut) {
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
        this.telephone = telephone;
        this.statut = statut;
    }

    /**
     * Obtient le nom du passager.
     *
     * @return Le nom du passager.
     */
    public String getNom() {
        return nom;
    }

    /**
     * Définit le nom du passager.
     *
     * @param nom Le nouveau nom du passager.
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Obtient le prénom du passager.
     *
     * @return Le prénom du passager.
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * Définit le prénom du passager.
     *
     * @param prenom Le nouveau prénom du passager.
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * Obtient la date de naissance du passager.
     *
     * @return La date de naissance du passager.
     */
    public String getDateNaissance() {
        return dateNaissance;
    }

    /**
     * Définit la date de naissance du passager.
     *
     * @param dateNaissance La nouvelle date de naissance du passager.
     */
    public void setDateNaissance(String dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    /**
     * Obtient le numéro de téléphone du passager.
     *
     * @return Le numéro de téléphone du passager.
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * Définit le numéro de téléphone du passager.
     *
     * @param telephone Le nouveau numéro de téléphone du passager.
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * Obtient le statut du passager.
     *
     * @return Le statut du passager.
     */
    public String getStatut() {
        return statut;
    }

    /**
     * Définit le statut du passager.
     *
     * @param statut Le nouveau statut du passager.
     */
    public void setStatut(String statut) {
        this.statut = statut;
    }

    /**
     * Représentation sous forme de chaîne de caractères du passager.
     *
     * @return Une chaîne de caractères représentant le passager.
     */
    @Override
    public String toString() {
        return "Passager{" +
                "nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", dateNaissance='" + dateNaissance + '\'' +
                ", telephone='" + telephone + '\'' +
                ", statut='" + statut + '\'' +
                '}';
    }
}
