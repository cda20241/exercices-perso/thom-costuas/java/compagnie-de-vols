import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        // Initialisation des listes
        List<Siege> lstSieges = new ArrayList<Siege>();
        List<Pilote> lstPilotes = new ArrayList<Pilote>();
        List<Vol> lstVols = new ArrayList<Vol>();

        // Création de deux avions
        Avion Airbus = new Avion("1", "AIRBUS", "A320");
        Avion Boeing = new Avion("2", "BOEING", "777");

        //System.out.println(Airbus.toString());
        //System.out.println(Boeing.toString());

        // Affichage des sièges des avions
        System.out.println(Airbus.getLstSieges().toString()+"\n");
        System.out.println(Boeing.getLstSieges().toString()+"\n");

        // Création d'un pilote
        Pilote P1 = new Pilote("P1", "James", "Arthur", "Commandant");
        //System.out.println(P1.toString());
        lstPilotes.add(P1);

        // Création d'une compagnie
        Compagnie ABC = new Compagnie("1", "ABC", "Paris", null, lstPilotes);
        //System.out.println(ABC.toString());

        // Création de deux vols
        Vol v1 = new Vol("Paris-Afrique", "2024/01/11", "2024/01/11", "08h00", "10h00", "Paris", "Afrique", "0", P1, Airbus);
        Vol v2 = new Vol("Afrique-Paris", "2024/01/12", "2024/01/12", "18h00", "20h00", "Afrique", "Paris", "0", P1, Boeing);
        //System.out.println(v1.toString());


        // Affichage des informations du vol v1
        System.out.println(v1.toString()+"\n");

        // Ajout des vols à la liste des vols de la compagnie ABC
        lstVols.add(v1);
        lstVols.add(v2);
        ABC.setLstVols(lstVols);

        // Création de deux passagers
        Passager Moi = new Passager("Pierre", "Paul", "2000/01/01", "0102030405", "aucun");
        Passager Paolo = new Passager("Paolo", "Truc", "1995/02/02", "0908070605", "aucun");

        // Réservation de billets pour les passagers sur le vol v1
        ABC.ReserverBillet("Paris-Afrique", "2024/01/11", Moi);
        ABC.ReserverBillet("Paris-Afrique", "2024/01/11", Paolo);
        //ABC.ReserverBillet("Afrique-Paris","2024/01/12",Moi);

        // Affrètement d'un nouvel avion (Boeing) pour le vol v1
        v1.affreterNouvelAvion(Boeing);
    }
}
