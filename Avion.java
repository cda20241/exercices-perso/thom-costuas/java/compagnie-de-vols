import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Classe représentant un avion avec ses caractéristiques et ses sièges.
 */
public class Avion {

    // Attributs de la classe Avion
    private String code;
    private String type;
    private String modele;
    private int nbPassagers;

    // Liste des sièges dans l'avion
    private List<Siege> lstSieges;

    /**
     * Constructeur de la classe Avion.
     *
     * @param code   Code de l'avion.
     * @param type   Type de l'avion.
     * @param modele Modèle de l'avion.
     */
    public Avion(String code, String type, String modele) {
        this.code = code;
        this.type = type;
        this.modele = modele;
        this.lstSieges = new ArrayList<>();

        // Demander à l'utilisateur de choisir le nombre d'allées
        Scanner scanner = new Scanner(System.in);
        System.out.print("\nChoisissez nombre d'allées : ");
        String userInput = scanner.nextLine();
        int nbAllees = Integer.valueOf(userInput);

        // Demander à l'utilisateur de choisir le nombre de rangées
        scanner = new Scanner(System.in);
        System.out.print("Choisissez nombre de rangées : ");
        userInput = scanner.nextLine();
        int nbRangees = Integer.valueOf(userInput);

        // Créer les sièges avec des lettres de l'alphabet pour les allées et des chiffres pour les rangées
        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        for (int i = 0; i < nbAllees; i++) {
            for (int j = 0; j < nbRangees; j++) {
                Siege unSiege = new Siege(String.valueOf(alphabet.charAt(i)), String.valueOf(j), "Economique");
                this.lstSieges.add(unSiege);
            }
        }

        // Calculer le nombre total de passagers
        this.nbPassagers = nbAllees * nbRangees;
    }

    // Méthodes d'accès aux attributs de la classe

    public List<Siege> getLstSieges() {
        return lstSieges;
    }

    public void setLstSieges(List<Siege> lstSieges) {
        this.lstSieges = lstSieges;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getModele() {
        return modele;
    }

    public void setModele(String modele) {
        this.modele = modele;
    }

    public int getNbPassagers() {
        return nbPassagers;
    }

    public void setNbPassagers(int nbPassagers) {
        this.nbPassagers = nbPassagers;
    }

    /**
     * Représentation sous forme de chaîne de caractères de l'avion.
     *
     * @return Une chaîne de caractères représentant l'avion.
     */
    @Override
    public String toString() {
        return "Avion{" +
                "code='" + code + '\'' +
                ", type='" + type + '\'' +
                ", modele='" + modele + '\'' +
                ", nbPassagers=" + nbPassagers +
                ", lstSieges=\n" + lstSieges +
                '}';
    }
}
