/**
 * Classe représentant un pilote.
 */
public class Pilote {

    // Attributs de la classe Pilote
    private String Matricule;
    private String Nom;
    private String Prenom;
    private String Qualif;

    /**
     * Constructeur de la classe Pilote.
     *
     * @param matricule Le matricule du pilote.
     * @param nom       Le nom du pilote.
     * @param prenom    Le prénom du pilote.
     * @param qualif    La qualification du pilote.
     */
    public Pilote(String matricule, String nom, String prenom, String qualif) {
        Matricule = matricule;
        Nom = nom;
        Prenom = prenom;
        Qualif = qualif;
    }

    /**
     * Obtient le matricule du pilote.
     *
     * @return Le matricule du pilote.
     */
    public String getMatricule() {
        return Matricule;
    }

    /**
     * Définit le matricule du pilote.
     *
     * @param matricule Le nouveau matricule du pilote.
     */
    public void setMatricule(String matricule) {
        Matricule = matricule;
    }

    /**
     * Obtient le nom du pilote.
     *
     * @return Le nom du pilote.
     */
    public String getNom() {
        return Nom;
    }

    /**
     * Définit le nom du pilote.
     *
     * @param nom Le nouveau nom du pilote.
     */
    public void setNom(String nom) {
        Nom = nom;
    }

    /**
     * Obtient le prénom du pilote.
     *
     * @return Le prénom du pilote.
     */
    public String getPrenom() {
        return Prenom;
    }

    /**
     * Définit le prénom du pilote.
     *
     * @param prenom Le nouveau prénom du pilote.
     */
    public void setPrenom(String prenom) {
        Prenom = prenom;
    }

    /**
     * Obtient la qualification du pilote.
     *
     * @return La qualification du pilote.
     */
    public String getQualif() {
        return Qualif;
    }

    /**
     * Définit la qualification du pilote.
     *
     * @param qualif La nouvelle qualification du pilote.
     */
    public void setQualif(String qualif) {
        Qualif = qualif;
    }

    /**
     * Représentation sous forme de chaîne de caractères du pilote.
     *
     * @return Une chaîne de caractères représentant le pilote.
     */
    @Override
    public String toString() {
        return "Pilote{" +
                "Matricule='" + Matricule + '\'' +
                ", Nom='" + Nom + '\'' +
                ", Prenom='" + Prenom + '\'' +
                ", Qualif='" + Qualif + '\'' +
                '}';
    }
}
