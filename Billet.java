/**
 * Classe représentant un billet associé à un vol pour un passager.
 */
public class Billet {

    // Attributs de la classe Billet
    private String numBillet;
    private String DateEmission;
    private String DateReservation;
    private String DatePaiement;

    // Attributs associés à d'autres classes
    private Siege SiegeReserve;
    private Passager lePassager;

    /**
     * Constructeur de la classe Billet.
     *
     * @param numBillet       Numéro unique du billet.
     * @param dateEmission    Date d'émission du billet.
     * @param dateReservation Date de réservation du billet.
     * @param datePaiement    Date de paiement du billet.
     * @param siegeReserve    Siège réservé pour ce billet.
     * @param lePassager      Passager associé à ce billet.
     */
    public Billet(String numBillet, String dateEmission, String dateReservation, String datePaiement, Siege siegeReserve, Passager lePassager) {
        this.numBillet = numBillet;
        DateEmission = dateEmission;
        DateReservation = dateReservation;
        DatePaiement = datePaiement;
        SiegeReserve = siegeReserve;
        this.lePassager = lePassager;
    }

    /**
     * Obtient le passager associé à ce billet.
     *
     * @return Le passager associé à ce billet.
     */
    public Passager getLePassager() {
        return lePassager;
    }

    /**
     * Définit le passager associé à ce billet.
     *
     * @param lePassager Le nouveau passager associé à ce billet.
     */
    public void setLePassager(Passager lePassager) {
        this.lePassager = lePassager;
    }

    /**
     * Obtient le siège réservé pour ce billet.
     *
     * @return Le siège réservé pour ce billet.
     */
    public Siege getSiegeReserve() {
        return SiegeReserve;
    }

    /**
     * Définit le siège réservé pour ce billet.
     *
     * @param siegeReserve Le nouveau siège réservé pour ce billet.
     */
    public void setSiegeReserve(Siege siegeReserve) {
        SiegeReserve = siegeReserve;
    }

    /**
     * Obtient le numéro unique du billet.
     *
     * @return Le numéro unique du billet.
     */
    public String getNumBillet() {
        return numBillet;
    }

    /**
     * Définit le numéro unique du billet.
     *
     * @param numBillet Le nouveau numéro unique du billet.
     */
    public void setNumBillet(String numBillet) {
        this.numBillet = numBillet;
    }

    /**
     * Obtient la date d'émission du billet.
     *
     * @return La date d'émission du billet.
     */
    public String getDateEmission() {
        return DateEmission;
    }

    /**
     * Définit la date d'émission du billet.
     *
     * @param dateEmission La nouvelle date d'émission du billet.
     */
    public void setDateEmission(String dateEmission) {
        DateEmission = dateEmission;
    }

    /**
     * Obtient la date de réservation du billet.
     *
     * @return La date de réservation du billet.
     */
    public String getDateReservation() {
        return DateReservation;
    }

    /**
     * Définit la date de réservation du billet.
     *
     * @param dateReservation La nouvelle date de réservation du billet.
     */
    public void setDateReservation(String dateReservation) {
        DateReservation = dateReservation;
    }

    /**
     * Obtient la date de paiement du billet.
     *
     * @return La date de paiement du billet.
     */
    public String getDatePaiement() {
        return DatePaiement;
    }

    /**
     * Définit la date de paiement du billet.
     *
     * @param datePaiement La nouvelle date de paiement du billet.
     */
    public void setDatePaiement(String datePaiement) {
        DatePaiement = datePaiement;
    }

    /**
     * Représentation sous forme de chaîne de caractères du billet.
     *
     * @return Une chaîne de caractères représentant le billet.
     */
    @Override
    public String toString() {
        return "Billet{" +
                "numBillet='" + numBillet + '\'' +
                ", DateEmission='" + DateEmission + '\'' +
                ", DateReservation='" + DateReservation + '\'' +
                ", DatePaiement='" + DatePaiement + '\'' +
                ", SiegeReserve=" + SiegeReserve +
                ", lePassager=" + lePassager +
                '}';
    }
}
