/**
 * Classe représentant un siège dans un avion.
 */
public class Siege {

    // Attributs de la classe Siege
    private String nomAllee;
    private String nomRang;
    private String classe;

    /**
     * Constructeur de la classe Siege.
     *
     * @param nomAllee La lettre représentant l'allée du siège.
     * @param nomRang  Le chiffre représentant le rang du siège.
     * @param classe   La classe du siège (par exemple, "Économie" ou "Affaires").
     */
    public Siege(String nomAllee, String nomRang, String classe) {
        this.nomAllee = nomAllee;
        this.nomRang = nomRang;
        this.classe = classe;
    }

    /**
     * Obtient la lettre représentant l'allée du siège.
     *
     * @return La lettre représentant l'allée du siège.
     */
    public String getNomAllee() {
        return nomAllee;
    }

    /**
     * Définit la lettre représentant l'allée du siège.
     *
     * @param nomAllee La nouvelle lettre représentant l'allée du siège.
     */
    public void setNomAllee(String nomAllee) {
        this.nomAllee = nomAllee;
    }

    /**
     * Obtient le chiffre représentant le rang du siège.
     *
     * @return Le chiffre représentant le rang du siège.
     */
    public String getNomRang() {
        return nomRang;
    }

    /**
     * Définit le chiffre représentant le rang du siège.
     *
     * @param nomRang Le nouveau chiffre représentant le rang du siège.
     */
    public void setNomRang(String nomRang) {
        this.nomRang = nomRang;
    }

    /**
     * Obtient la classe du siège.
     *
     * @return La classe du siège.
     */
    public String getClasse() {
        return classe;
    }

    /**
     * Définit la classe du siège.
     *
     * @param classe La nouvelle classe du siège.
     */
    public void setClasse(String classe) {
        this.classe = classe;
    }

    /**
     * Représentation sous forme de chaîne de caractères du siège.
     *
     * @return Une chaîne de caractères représentant le siège.
     */
    @Override
    public String toString() {
        return "nomSiege='" + nomAllee + nomRang + '\'' +
                ", classe='" + classe;
    }
}
