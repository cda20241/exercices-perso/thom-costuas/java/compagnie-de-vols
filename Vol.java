import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Classe représentant un vol avec ses caractéristiques, son équipage, son avion et ses réservations.
 */
public class Vol {

    // Attributs de la classe Vol
    private String NomVol;
    private String DateDepart;
    private String DateArrivee;
    private String HeureDepart;
    private String HeureArrivee;
    private String VilleDepart;
    private String VilleArrivee;
    private String Retard;

    private Pilote lePilote;
    private Avion lAvion;

    // Liste des réservations pour le vol
    private List<Billet> lstReservations;

    /**
     * Constructeur de la classe Vol.
     *
     * @param nomVol       Nom du vol.
     * @param dateDepart   Date de départ.
     * @param dateArrivee  Date d'arrivée.
     * @param heureDepart  Heure de départ.
     * @param heureArrivee Heure d'arrivée.
     * @param villeDepart  Ville de départ.
     * @param villeArrivee Ville d'arrivée.
     * @param retard       Retard éventuel du vol.
     * @param lePilote     Pilote du vol.
     * @param lAvion       Avion du vol.
     */
    public Vol(String nomVol, String dateDepart, String dateArrivee, String heureDepart, String heureArrivee,
               String villeDepart, String villeArrivee, String retard, Pilote lePilote, Avion lAvion) {
        NomVol = nomVol;
        DateDepart = dateDepart;
        DateArrivee = dateArrivee;
        HeureDepart = heureDepart;
        HeureArrivee = heureArrivee;
        VilleDepart = villeDepart;
        VilleArrivee = villeArrivee;
        Retard = retard;
        this.lePilote = lePilote;
        this.lAvion = lAvion;
        this.lstReservations = new ArrayList<>();
    }

    // Méthodes d'accès aux attributs de la classe

    public List<Billet> getLstReservations() {
        return lstReservations;
    }

    public void setLstReservations(List<Billet> lstReservations) {
        this.lstReservations = lstReservations;
    }

    public Pilote getLePilote() {
        return lePilote;
    }

    public void setLePilote(Pilote lePilote) {
        this.lePilote = lePilote;
    }

    public Avion getlAvion() {
        return lAvion;
    }

    public void setlAvion(Avion lAvion) {
        this.lAvion = lAvion;
    }

    public String getNomVol() {
        return NomVol;
    }

    public void setNomVol(String nomVol) {
        NomVol = nomVol;
    }

    public String getDateDepart() {
        return DateDepart;
    }

    public void setDateDepart(String dateDepart) {
        DateDepart = dateDepart;
    }

    public String getDateArrivee() {
        return DateArrivee;
    }

    public void setDateArrivee(String dateArrivee) {
        DateArrivee = dateArrivee;
    }

    public String getHeureDepart() {
        return HeureDepart;
    }

    public void setHeureDepart(String heureDepart) {
        HeureDepart = heureDepart;
    }

    public String getHeureArrivee() {
        return HeureArrivee;
    }

    public void setHeureArrivee(String heureArrivee) {
        HeureArrivee = heureArrivee;
    }

    public String getVilleDepart() {
        return VilleDepart;
    }

    public void setVilleDepart(String villeDepart) {
        VilleDepart = villeDepart;
    }

    public String getVilleArrivee() {
        return VilleArrivee;
    }

    public void setVilleArrivee(String villeArrivee) {
        VilleArrivee = villeArrivee;
    }

    public String getRetard() {
        return Retard;
    }

    public void setRetard(String retard) {
        Retard = retard;
    }

    /**
     * Affiche les sièges disponibles pour la réservation.
     */
    public void afficherSiegesDisponible() {
        List<Billet> lstReserv = this.getLstReservations();
        for (Siege unSiege : this.getlAvion().getLstSieges()) {
            Boolean estLibre = true;
            for (Billet uneReservation : lstReserv) {
                if (Objects.equals(uneReservation.getSiegeReserve(), unSiege)) {
                    estLibre = false;
                    break;
                }
            }
            if (estLibre) {
                System.out.println(unSiege.toString());
            }
        }
        System.out.println("\n");
    }

    /**
     * Affrète un nouvel avion pour le vol en transférant les réservations du vol actuel au nouvel avion.
     *
     * @param nouvelAvion Nouvel avion à affréter.
     */
    public void affreterNouvelAvion(Avion nouvelAvion) {
        List<Billet> lstReservAnciennes = this.getLstReservations();
        List<Billet> lstReservNouvelles = new ArrayList<>();

        for (Siege unSiege : nouvelAvion.getLstSieges()) {
            Boolean estLibre = true;
            for (Billet ancienBillet : lstReservAnciennes) {
                for (Billet uneReservation : lstReservNouvelles) {
                    if (Objects.equals(uneReservation.getSiegeReserve(), unSiege)) {
                        estLibre = false;
                        break;
                    }
                }
                if (estLibre) {
                    lstReservNouvelles.add(ancienBillet);
                    lstReservAnciennes.remove(ancienBillet);
                    break;
                }
            }
        }

        if (lstReservAnciennes.isEmpty()) {
            System.out.println("Tout le monde a été transféré avec succès !");
        } else {
            System.out.println("Des passagers n'ont pas pu être transférés !");
            for (Billet unBillet : lstReservAnciennes) {
                System.out.println("Passager : " + unBillet.getLePassager().getPrenom() + " " + unBillet.getLePassager().getNom());
            }
        }

        this.setLstReservations(lstReservNouvelles);
        this.setlAvion(nouvelAvion);
    }


    @Override
    public String toString() {
        return "Vol{" +
                "NomVol='" + NomVol + '\'' +
                ", DateDepart='" + DateDepart + '\'' +
                ", DateArrivee='" + DateArrivee + '\'' +
                ", HeureDepart='" + HeureDepart + '\'' +
                ", HeureArrivee='" + HeureArrivee + '\'' +
                ", VilleDepart='" + VilleDepart + '\'' +
                ", VilleArrivee='" + VilleArrivee + '\'' +
                ", Retard='" + Retard + '\'' +
                ", lePilote=" + lePilote +
                ", lAvion=" + lAvion +
                ", lstReservations=" + lstReservations +
                '}';
    }
}
