// Import des classes nécessaires
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

/**
 * Classe représentant une compagnie aérienne avec ses caractéristiques, ses vols et ses pilotes.
 */
public class Compagnie {

    // Attributs de la classe Compagnie
    private String code;
    private String Nom;
    private String SiègeSocial;

    private List<Vol> lstVols;
    private List<Pilote> lstPilotes;

    /**
     * Constructeur de la classe Compagnie.
     *
     * @param code         Code de la compagnie.
     * @param nom          Nom de la compagnie.
     * @param siègeSocial  Siège social de la compagnie.
     * @param lstVols      Liste des vols de la compagnie.
     * @param lstPilotes   Liste des pilotes de la compagnie.
     */
    public Compagnie(String code, String nom, String siègeSocial, List<Vol> lstVols, List<Pilote> lstPilotes) {
        this.code = code;
        Nom = nom;
        SiègeSocial = siègeSocial;
        this.lstVols = lstVols;
        this.lstPilotes = lstPilotes;
    }

    // Méthodes d'accès aux attributs de la classe

    public List<Vol> getLstVols() {
        return lstVols;
    }

    public void setLstVols(List<Vol> lstVols) {
        this.lstVols = lstVols;
    }

    public List<Pilote> getLstPilotes() {
        return lstPilotes;
    }

    public void setLstPilotes(List<Pilote> lstPilotes) {
        this.lstPilotes = lstPilotes;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNom() {
        return Nom;
    }

    public void setNom(String nom) {
        Nom = nom;
    }

    public String getSiègeSocial() {
        return SiègeSocial;
    }

    public void setSiègeSocial(String siègeSocial) {
        SiègeSocial = siègeSocial;
    }

    /**
     * Réserve un billet pour un passager sur un vol spécifié.
     *
     * @param nomVol      Nom du vol.
     * @param dateDepart  Date de départ du vol.
     * @param lePassager  Passager pour lequel la réservation est effectuée.
     * @return True si la réservation a réussi, False sinon.
     */
    public Boolean ReserverBillet(String nomVol, String dateDepart, Passager lePassager) {
        for (Vol unVol : lstVols) {
            if (Objects.equals(unVol.getNomVol(), nomVol) && Objects.equals(unVol.getDateDepart(), dateDepart)) {
                unVol.afficherSiegesDisponible();

                boolean ok = false;
                String nomAlleeSaisie = null, nomRangSaisie = null;
                while (!ok) {
                    Scanner scanner = new Scanner(System.in);
                    System.out.print("Choisissez votre siège : ");
                    String userInput = scanner.nextLine();
                    if (userInput.length() == 2) {
                        nomAlleeSaisie = String.valueOf(userInput.charAt(0));
                        nomRangSaisie = String.valueOf(userInput.charAt(1));
                        ok = true;
                        System.out.println("Première lettre : " + nomAlleeSaisie);
                        System.out.println("Deuxième lettre : " + nomRangSaisie);
                    } else {
                        System.out.println("Nom de siège invalide !");
                    }
                }

                List<Billet> lstReserv = unVol.getLstReservations();
                for (Siege unSiege : unVol.getlAvion().getLstSieges()) {
                    if (Objects.equals(unSiege.getNomAllee(), nomAlleeSaisie) && Objects.equals(unSiege.getNomRang(), nomRangSaisie)) {
                        Boolean estLibre = true;
                        for (Billet uneReservation : lstReserv) {
                            if (Objects.equals(uneReservation.getSiegeReserve(), unSiege)) {
                                estLibre = false;
                                break;
                            }
                        }
                        if (estLibre) {
                            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                            Date date = new Date(timestamp.getTime());
                            SimpleDateFormat dateFormat = new SimpleDateFormat("HH'h'mm'm'");
                            String dateEmission, dateReservation, datePaiment;
                            dateEmission = dateReservation = datePaiment = dateFormat.format(date);

                            String code = String.valueOf(lePassager.getNom().charAt(0) + lePassager.getPrenom().charAt(0));

                            Billet billetReservation = new Billet(code, dateEmission, dateReservation, datePaiment, unSiege, lePassager);
                            lstReserv.add(billetReservation);
                            unVol.setLstReservations(lstReserv);
                            System.out.println("Réservation réussie ! ");
                            return true;
                        }
                    }
                }
                System.out.println("Le siège spécifié n'est pas disponible pour ce vol ! ");
                return false;
            }
        }
        System.out.println("Aucun vol ne correspond à votre recherche ! ");
        return false;
    }





//La reservation se fait par rapport au premier siège disponible.
//    public Boolean ReserverBillet(String nomVol, String dateDepart, Passager lePassager){
//        for (Vol unVol : lstVols){
//            if(unVol.getNomVol() == nomVol && unVol.getDateDepart()== dateDepart)
//            {
//                List<Billet> lstReserv = unVol.getLstReservations();
//                for (Siege unSiege : unVol.getlAvion().getLstSieges())
//                {
//                    Boolean estLibre = true;
//                    for(Billet uneReservation : lstReserv)
//                    {
//                        if (uneReservation.getSiegeReserve() == unSiege)
//                        {
//                            estLibre = false;
//                            break;
//                        }
//                    }
//                    if(estLibre)
//                    {
//                        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
//                        Date date = new Date(timestamp.getTime());
//                        SimpleDateFormat dateFormat = new SimpleDateFormat("HH'h'mm'm'");
//                        String dateEmission,dateReservation,datePaiment;
//                        dateEmission = dateReservation = datePaiment = dateFormat.format(date);
//
//                        Billet billetReservation = new Billet("1",dateEmission, dateReservation,datePaiment,unSiege);
//                        lstReserv.add(billetReservation);
//                        unVol.setLstReservations(lstReserv);
//                        return true;
//                    }
//                }
//                System.out.println("Aucun siège disponible pour ce vol ! ");
//                return false;
//            }
//        }
//        System.out.println("Aucun vol ne correspond à votre recherche ! ");
//        return false;
//    }


}
