import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        
        boolean exit = false;
        System.out.println("Bienvenue dans notre compagnie aérienne ! \n");
        newGame.getResults();

        while (!exit) {
            System.out.println("\n");
            System.out.println("1. Réserver un billet");
            System.out.println("2. Afficher les sièges disponible");
            System.out.println("3. Consulter le tableau des scores");
            System.out.println("4. Quitter la partie");
            Scanner scanner = new Scanner(System.in);
            System.out.print("\nChoisissez une option valide : ");
            String selectedOption = scanner.next();

            if (selectedOption.equals("1")) {
                clearScreen();
                boolean ok = false;
                while (!ok) {
                    System.out.print("\nChoisissez votre nom (la partie démarrera après la sélection du nom) : ");
                    String name = scanner.next();

                    if (name.equals("exit")) {
                        System.out.println("Ce nom est invalide ! ");
                    } else {
                        ok = true;
                        newGame.setPlayerName(name);
                    }
                }

                clearScreen();
                newGame.generateRandomValue();
                newGame.startChrono();
                boolean win = false;

                while (!win) {
                    clearScreen();
                    System.out.print("Votre choix ---> ");
                    int tentative = scanner.nextInt();
                    win = newGame.plusOuMoins(tentative);
                }

                newGame.addResults(newGame.getPlayerName() + " : " + newGame.getScore() + "\n");
                System.out.println(newGame.getPlayerName() + " : " + newGame.getScore());

            } else if (selectedOption.equals("2")) {
                clearScreen();
                boolean exitOptions = false;
                System.out.println("Que voulez-vous modifier ? \n");

                while (!exitOptions) {
                    System.out.println("1. Modifier valeur maximale");
                    System.out.println("2. Modifier valeur minimale");
                    System.out.println("3. Revenir au menu");
                    System.out.print("\nChoisissez une option valide : ");
                    String option = scanner.next();

                    if (option.equals("1")) {
                        System.out.print("\nNouvelle valeur maximale : ");
                        int val = scanner.nextInt();
                        newGame.setValeurMax(val);
                        exitOptions = true;

                    } else if (option.equals("2")) {
                        System.out.print("\nNouvelle valeur minimale : ");
                        int val = scanner.nextInt();
                        newGame.setValeurMin(val);
                        exitOptions = true;

                    } else if (option.equals("3")) {
                        exitOptions = true;
                        clearScreen();

                    } else {
                        System.out.println("\nChoix invalide ! \n");
                    }
                }

            } else if (selectedOption.equals("3")) {
                clearScreen();
                boolean exitResultat = false;
                System.out.println("\n\nMenu de recherche des résultats: \n");

                List<String> data = newGame.searchResults();

                while (!exitResultat) {
                    System.out.print("\nVotre recherche (tapez 'exit' pour quitter) : ");
                    String recherche = scanner.next();
                    if (recherche.toLowerCase().equals("exit")) {
                        exitResultat = true;
                        clearScreen();
                    } else {
                        List<String> resultatsTrouves = new ArrayList<>();
                        for (String ligne : data) {
                            if (ligne.toLowerCase().contains(recherche.toLowerCase())) {
                                resultatsTrouves.add(ligne);
                            }
                        }

                        if (!resultatsTrouves.isEmpty()) {
                            System.out.println();
                            for (String resultat : resultatsTrouves) {
                                System.out.println(resultat);
                            }
                        } else {
                            System.out.println("Aucun résultat trouvé.");
                        }
                    }
                }

            } else if (selectedOption.equals("4")) {
                System.out.println("\nAu revoir");
                exit = true;

            } else {
                System.out.println("\nChoix invalide ! \n");
            }
        }
    }
}


















//La reservation se fait par rapport au premier siège disponible.
//    public Boolean ReserverBillet(String nomVol, String dateDepart, Passager lePassager){
//        for (Vol unVol : lstVols){
//            if(unVol.getNomVol() == nomVol && unVol.getDateDepart()== dateDepart)
//            {
//                List<Billet> lstReserv = unVol.getLstReservations();
//                for (Siege unSiege : unVol.getlAvion().getLstSieges())
//                {
//                    Boolean estLibre = true;
//                    for(Billet uneReservation : lstReserv)
//                    {
//                        if (uneReservation.getSiegeReserve() == unSiege)
//                        {
//                            estLibre = false;
//                            break;
//                        }
//                    }
//                    if(estLibre)
//                    {
//                        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
//                        Date date = new Date(timestamp.getTime());
//                        SimpleDateFormat dateFormat = new SimpleDateFormat("HH'h'mm'm'");
//                        String dateEmission,dateReservation,datePaiment;
//                        dateEmission = dateReservation = datePaiment = dateFormat.format(date);
//
//                        Billet billetReservation = new Billet("1",dateEmission, dateReservation,datePaiment,unSiege);
//                        lstReserv.add(billetReservation);
//                        unVol.setLstReservations(lstReserv);
//                        return true;
//                    }
//                }
//                System.out.println("Aucun siège disponible pour ce vol ! ");
//                return false;
//            }
//        }
//        System.out.println("Aucun vol ne correspond à votre recherche ! ");
//        return false;
//    }



















































import javax.swing.text.AttributeSet;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        List<Siege> lstSieges = new ArrayList<Siege>();
        List<Pilote> lstPilotes = new ArrayList<Pilote>();
        List<Vol> lstVols = new ArrayList<Vol>();

        Avion Airbus = new Avion("1","AIRBUS","A320");
        //System.out.println(Airbus.toString());

        Avion Boeing = new Avion("2","BOEING", "777");
        //System.out.println(Boeing.toString());

        System.out.println(Airbus.getLstSieges().toString());
        System.out.println(Boeing.getLstSieges().toString());


        Pilote P1 = new Pilote("P1","James","Arthur","Commandant");
        lstPilotes.add(P1);
        //System.out.println(P1.toString());

        Compagnie ABC = new Compagnie("1","ABC","Paris",null,lstPilotes);
        //System.out.println(ABC.toString());


        Vol v1 = new Vol("Paris-Afrique","2024/01/11","2024/01/11", "08h00","10h00","Paris","Afrique","0",P1,Airbus);
        Vol v2 = new Vol("Afrique-Paris","2024/01/12","2024/01/12", "18h00","20h00","Afrique","Paris","0",P1,Boeing);
        //System.out.println(v1.toString());

        lstVols.add(v1);
        lstVols.add(v2);

        ABC.setLstVols(lstVols);

        Passager Moi = new Passager("Pierre", "Paul","2000/01/01","0102030405","aucun");
        Passager Paolo = new Passager("Paolo", "Truc","1995/02/02","0908070605","aucun");

        ABC.ReserverBillet("Paris-Afrique","2024/01/11",Moi);
        ABC.ReserverBillet("Paris-Afrique","2024/01/11",Paolo);
        //ABC.ReserverBillet("Afrique-Paris","2024/01/12",Moi);

        v1.affreterNouvelAvion(Boeing);


    }



}


